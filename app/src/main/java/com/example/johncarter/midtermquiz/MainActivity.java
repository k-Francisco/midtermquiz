package com.example.johncarter.midtermquiz;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {


    private Switch mSwitch;
    private Button mTrue, mFalse;
    String[] tvColors = {"red", "blue", "yellow"};
    String[] frameColors = {"red", "blue", "yellow"};
    private TextView color, frame, countDown, score;
    Thread oyeye;
    int n,m, scorer=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwitch = (Switch) findViewById(R.id.switches);
        mSwitch.setOnCheckedChangeListener(this);
        mTrue = (Button) findViewById(R.id.btnTrue);
        mFalse = (Button) findViewById(R.id.btnFalse);
        mTrue.setOnClickListener(this);
        mFalse.setOnClickListener(this);
        color = (TextView) findViewById(R.id.color);
        frame = (TextView) findViewById(R.id.colorFrame);
        countDown = (TextView) findViewById(R.id.countDown);
        score = (TextView) findViewById(R.id.score);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
         oyeye = new Thread(new Timer());

        if(isChecked == true){
//            Toast.makeText(this, "wawaa", Toast.LENGTH_SHORT).show();
            mTrue.setEnabled(true);
            mFalse.setEnabled(true);

            Random rand = new Random();
             n = rand.nextInt(4);
             m = rand.nextInt(4);

            if(n == 1){
                color.setText("RED");
            }
            else if (n == 2){
                color.setText("BLUE");
            }
            else if(n == 3){
                color.setText("YELLOW");
            }

            if(m == 1){
                frame.setBackgroundColor(Color.parseColor("#ff0000"));
            }
            else if (m == 2){
                frame.setBackgroundColor(Color.parseColor("#0000ff"));;
            }
            else if(m == 3){
                frame.setBackgroundColor(Color.parseColor("#ffff00"));
            }


            if (oyeye.getState() == Thread.State.NEW)
            {
                oyeye.start();
            }

        }
        else if(isChecked == false){


            if (oyeye!=null && oyeye.isAlive())
            {
                oyeye.interrupt();
            }

            mTrue.setEnabled(false);
            mFalse.setEnabled(false);

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnTrue:
                mSwitch.setChecked(false);
                if(n==m){

                    scorer+=1;
                    score.setText(scorer+"");
                    oyeye.interrupt();
                    mTrue.setEnabled(false);
                    mFalse.setEnabled(false);

                }
                else {
                    scorer-=1;
                    score.setText(scorer+"");
                }
                mTrue.setEnabled(false);
                mFalse.setEnabled(false);
                break;
            case R.id.btnFalse:
                mSwitch.setChecked(false);
                if(n!=m){
                    scorer+=1;
                    score.setText(scorer+"");
                }
                else {
                    scorer-=1;
                    score.setText(scorer+"");
                }
               oyeye.interrupt();
                mTrue.setEnabled(false);
                mFalse.setEnabled(false);

                break;
        }
    }


    class Timer implements Runnable{
        int i;

        @Override
        public void run() {
            for ( i =0;i<10;i++){
                if (!mSwitch.isChecked())
                    break;
                try {
                    Thread.sleep(1000);
                if(oyeye==null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            countDown.setText(i + "");

                            if (i == 10) {
                                mTrue.setEnabled(false);
                                mFalse.setEnabled(false);
                                mSwitch.setChecked(false);
                                if (n == m) {
                                    scorer += 1;
                                    score.setText(scorer + "");
                                } else if (n!=m){
                                    scorer -= 1;
                                    score.setText(scorer + "");
                                }
                            }
                        }
                    });
                }
                    oyeye = null;

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
